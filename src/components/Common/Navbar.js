import React from 'react';
import styles from '../../Styles/Common/Navbar.module.scss'
import PropTypes from 'prop-types';
import { AppBar, Box, Divider, Drawer, IconButton, List, ListItem, ListItemButton, ListItemText, Toolbar, Typography, Button, Container} from '@mui/material';
import { Menu as MenuIcon } from "@material-ui/icons";
import  Blacklogo  from "../../images/logo.svg";

const drawerWidth = 240;
const navItems = ['Project', 'Blog', 'Portfolio', 'Service', 'Contact', 'About', 'Home',];

    function Navbar(props) {
      const { window } = props;
      const [mobileOpen, setMobileOpen] = React.useState(false);
    
      const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
      };
    
      const drawer = (
        <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
          <Container>
            <Typography variant="h6" sx={{ my: 2 }}>
              <img src={Blacklogo} alt="" />
            </Typography>
            <Divider />
            <List>
              {navItems.map((item) => (
                <ListItem key={item} disablePadding>
                  <ListItemButton sx={{ textAlign: 'center' }}>
                    <ListItemText primary={item} />
                  </ListItemButton>
                </ListItem>
              ))}
            </List>
          </Container>
        </Box>
      );
    
      const container = window !== undefined ? () => window().document.body : undefined;
    
      return (
        <Box sx={{ display: 'flex' }}>
          <AppBar className={styles.BGwhite}>
            <Box className={styles.custom_container}>
                <Toolbar className={styles.toolbar_main}>
                    <IconButton
                      color="inherit"
                      aria-label="open drawer"
                      edge="start"
                      onClick={handleDrawerToggle}
                      sx={{ mr: 2, display: { sm: 'none' } }}
                    >
                      <MenuIcon />
                    </IconButton>
                    <Typography
                      variant="h6"
                      component="div"
                      className={styles.logo}
                      sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
                    >
                      <img src={Blacklogo} alt="" />
                    </Typography>
                    <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
                      {navItems.map((item) => (
                        <Button key={item} sx={{ color: '#000' }}>
                          {item}
                        </Button>
                      ))}
                    </Box>
                  </Toolbar>
            </Box>  
          </AppBar>
          <Box component="nav">
            <Drawer
              container={container}
              variant="temporary"
              open={mobileOpen}
              onClose={handleDrawerToggle}
              ModalProps={{
                keepMounted: true, // Better open performance on mobile.
              }}
              sx={{
                display: { xs: 'block', sm: 'none' },
                '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
              }}
            >
              {drawer}
            </Drawer>
          </Box>
        </Box>  
  );
};

export default Navbar;