import React from 'react';
import {Box, Typography, Button, Grid, Container, item, Stack} from '@mui/material';
import styles from "../../Styles/Home/FreeQoute.module.scss"
const FreeQoute = () => {
    return (   
        <Box className={styles.freeQoute}>
            <Container>
                <Stack direction="row" justifyContent="space-between" alignItems="center">
                    <item item xs={8}>
                        <Typography variant="h2">Get a Quote For Your Project</Typography>
                    </item>
                    <item item xs={4}>
                        <Button variant="outlined"className={styles.qouteBtn}>Free Qoute</Button>
                    </item>
                </Stack>
            </Container>
        </Box>
     );
};

export default FreeQoute;