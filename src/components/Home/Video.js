import React from 'react';
import {Box, Typography, Button} from '@mui/material';
import VideoThumb from "../../images/videoThumb.svg"
import styles from "../../Styles/Home/Video.module.scss"
const Video = () => {
    return (
        <Box className={styles.videoMain}>
            <Box className={styles.videoLeft}>
                <Typography variant="h3">We’ve Been Building For Over 10 Years</Typography>
                <Typography variant="span">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sapien, dignissim tristique tellus sed faucibus nullam. Tincidunt mauris ut quam sed mauris proin feugiat. Scelerisque lorem posuere iaculis nunc amet phasellus.</Typography>
                <Button variant="outlined">About us</Button>
            </Box>
            <Box className={styles.videoRight}>
                <img src={VideoThumb} />
            </Box>
        </Box>
        )
    }
    
export default Video;