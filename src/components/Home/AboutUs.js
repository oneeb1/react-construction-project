import React from 'react';
import {Box, Typography, Button, Grid, Container, item, Stack} from '@mui/material';
import styles from "../../Styles/Home/AboutUs.module.scss"
import aboutUs from "../../images/aboutus.svg"

const about =[
    {
        sectionHeading:'No Project Too Big Or Too Small',
        leftDescription:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sapien, dignissim tristique tellus sed faucibus nullam. Tincidunt mauris ut quam sed mauris proin feugiat. Scelerisque lorem posuere iaculis nunc amet phasellus.<br /> <br /> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sapien, dignissim tristique tellus sed faucibus nullam.',
        rightDescription:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sapien, dignissim tristique tellus sed faucibus nullam. Tincidunt mauris ut quam sed mauris proin feugiat. Scelerisque lorem posuere iaculis nunc amet phasellus.',
    }
];

const AboutUs = () => {
    return (    
        <Box className={styles.aboutusMain}>
            <Box className={styles.aboutimage}>
                <img src={aboutUs} />
            </Box>
            <Box className={styles.aboutDescription}>
            {about.map((aboutus) => (
                <Box className={styles.descriptioninner}>
                    <Typography variant="h2">{aboutus.sectionHeading}</Typography>
                    <Box className={styles.aboutDescipt}>
                        <Stack direction="row" spacing={4}>
                            <Box className={styles.textdetail}>
                                <Typography variant="span" dangerouslySetInnerHTML={{__html: aboutus.leftDescription}}></Typography>
                            </Box>
                            <Box className={styles.textdetail}>
                                <Typography variant="span">
                                    {aboutus.rightDescription}
                                    <Button variant="outlined"className={styles.learnBtn}>Learn More</Button>
                                </Typography>
                            </Box>
                        </Stack>
                          
                    </Box>
                </Box>
            ))}
                <Box className={styles.aboutcontent}>
                    <Box className={styles.years}>
                        <Typography variant="h4">12</Typography>
                        <Typography variant="span">YEARS ESTABLISHED</Typography>
                    </Box> 
                    <Box className={styles.projects}>
                        <Typography variant="h4">250</Typography>
                        <Typography variant="span">COMPLETED PROJECTS</Typography>
                    </Box> 
                </Box>  
            </Box>
        </Box>
     );
};

export default AboutUs;