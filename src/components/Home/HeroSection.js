import React from 'react';
import {Box, Typography, Button, Grid} from '@mui/material';
import Slider from "../../images/slider.svg"
import Building from "../../images/icon_service.svg"
import Foundation from "../../images/foundation.svg"
import Site from "../../images/site.svg"
import styles from "../../Styles/Home/Slider.module.scss"

const services = [
    {
        imageName:Building,
        seriveName: "Building Construction",
        serviceDescription:"Lorem ipsum dolor sit consectetur adipiscing elit.",
    },
    {
        imageName:Foundation,
        seriveName: "Foundation Work",
        serviceDescription:"Lorem ipsum dolor sit consectetur adipiscing elit.",
    },
    {
        imageName:Site,
        seriveName: "Site Management",
        serviceDescription:"Lorem ipsum dolor sit consectetur adipiscing elit.",
    },
];

const HeroSection = () => {
    return (
        <Box className={styles.slider} sx={{ flexGrow: 1 }}>
            <Box className={styles.imagemain}>
                <img src={Slider} alt="" />
                <Box className={styles.sliderText}>
                    <Typography variant="h1">
                        Construction
                    </Typography>
                    <Typography variant="h6">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quis egestas pellentesque libero dolor in diam consequat ut. 
                    Mi nibh amet viverra id aliquet neque odio.</Typography>
                    <Button variant="contained">View Our Work</Button>
                </Box>
            </Box>
            <Box className={styles.leftside}>
                <Box className={styles.leftsideinner}>
                    <Typography variant="h3">Our Services</Typography>
                    {services.map((service) => (
                        <Box className={styles.services_listing}>
                            <Box className={styles.serviceimg}>
                                <img src={service.imageName} alt="" />
                            </Box>
                            <Box className={styles.servicedetail}>
                                <Typography variant="h4">{service.seriveName}</Typography>
                                <Typography variant="span">{service.serviceDescription}</Typography>
                            </Box>
                        </Box>
                    ))}    
                </Box>
            </Box>   
        </Box>
        
    );
};

export default HeroSection;