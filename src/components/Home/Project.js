import React from 'react'
import {Box, Typography, Button, Grid, Container} from '@mui/material';
import  Image1  from "../../images/projectImg.svg";
import  Image2  from "../../images/projectImg2.svg";
import  Image3  from "../../images/projectImg3.svg";
import styles from "../../Styles/Home/Project.module.scss"
const projects = [
    {
        imageName:Image1,
        projectTitle: "Project Title",
        projectDescription:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Id et euismod bibendum adipiscing et orci, fermentum. Cras tristique viverra gravida et sit egestas.",
        projectbtn:"View Project"
    },
    {
        imageName:Image2,
        projectTitle: "Project Title",
        projectDescription:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Id et euismod bibendum adipiscing et orci, fermentum. Cras tristique viverra gravida et sit egestas.",
        projectbtn:"View Project"
    },
    {
        imageName:Image3,
        projectTitle: "Project Title",
        projectDescription:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Id et euismod bibendum adipiscing et orci, fermentum. Cras tristique viverra gravida et sit egestas.",
        projectbtn:"View Project"
    },
    
];

const Project = () => {
    return (
        <Box>
            <Container>
                <Box className={styles.heading}>
                    <Typography variant="h2">Latest Projects</Typography>
                </Box>
                <Grid container rowSpacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
                {projects.map((project) => (
                    <Grid item xs={4}>
                        <Box className={styles.project_listing}>
                            <Box className={styles.projectimg}>
                                <img src={project.imageName} alt="" />
                            </Box>
                            <Box className={styles.projectdetail}>
                                <Typography variant="h4">{project.projectTitle}</Typography>
                                <Typography variant="span">{project.projectDescription}</Typography>
                                <Button className={styles.projectBtn}>{project.projectbtn}</Button>
                            </Box>
                        </Box>
                    </Grid>
                ))}  
                </Grid>
                <Box className={styles.moreBtn}>
                    <Button variant="outlined">View All</Button>
                </Box>
            </Container>
        </Box>
    )
}

export default Project;