import React from 'react';
import Navbar from '../components/Common/Navbar';
import Hero from '../components/Home/HeroSection'
import FreeQoute from '../components/Home/FreeQoute'
import AboutUs from '../components/Home/AboutUs'
import Project from '../components/Home/Project'
import Video from '../components/Home/Video'

const Home = () => {
    return (
        <div>
            <Navbar />
            <Hero />
            <FreeQoute />
            <AboutUs />
            <Project />
            <Video />
        </div>
    );
};

export default Home;